### Interactive Infographic
This project is based on Reveal.js: https://github.com/hakimel/reveal.js.git.

### Full setup

1. Install [Node.js](http://nodejs.org/) (4.0.0 or later)

1. Clone the repository
   ```sh
   $ git clone repositoryName
   ```

1. Navigate to the infographic folder
   ```sh
   $ cd infographic
   ```

1. Install dependencies
   ```sh
   $ npm install
   ```

1. Serve the presentation and monitor source files for changes
   ```sh
   $ npm start
   ```

1. Open <http://localhost:8000> to view your presentation

   You can change the port by using `npm start -- --port=8001`.


### Folder Structure
- **css/** Core styles without which the project does not function
- **js/** Like above but for JavaScript
- **img/** Image assets
- **plugin/** Components that have been developed as extensions to reveal.js
- **lib/** All other third party assets (JavaScript, CSS, fonts)

