var navContainer = document.getElementById("spots");

//initialise reveal 
Reveal.initialize({
	transition:'zoom', 
	progress: false,
	history: true,
	width: 960,
	controls: true,
	minScale: 0.5,
	maxScale: 3
});


//make circle navigation


var makeNav = function() {
	var totalSlides = Reveal.getTotalSlides();
	for (i = 0; i < totalSlides; i++)  {
		var spot = '<a href="#/slide' + i + '" class="spot">' + i + '</a> '
		navContainer.innerHTML += spot;
	}
	navContainer.getElementsByClassName('spot')[0].classList.add('current', 'first');
}

makeNav();

Reveal.addEventListener( 'slidechanged', function( event ) {
	var url = window.location.href;
	setTimeout(function() {
		var url = window.location.href;
		url = url.split('#')[1];
		var spots = navContainer.getElementsByClassName('spot');
		for (i = 0; i < spots.length; i++)  {
			spots[i].classList.remove('current');
			var href = spots[i].getAttribute('href');
			if (href.indexOf(url) >= 0) {
				spots[i].classList.add('current');
			}
		}
	}, 200);
	if (url === '/slide3') {
		document.getElementById('arrow-offset-right').classList.add('grow');
		document.getElementById('arrow-offset-left').classList.add('grow');
	}
} );

